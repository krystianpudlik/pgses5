(function () {
    'use strict';

    angular
        .module('pgsapp')
        .config(function ($stateProvider) {
            $stateProvider
                .state('app', {
                    url: '/',
                    template: '<app></app>'
                });
        });

})();