(function () {
    'use strict';
    
    angular
        .module('pgsapp', [
            'ui.router',
            'pgsapp.components'
        ])
        .config(config);

    function config($stateProvider, $urlRouterProvider, $locationProvider) {
        console.log('config appModule');
        $locationProvider
            .html5Mode(true)
            .hashPrefix('!');

        $urlRouterProvider.otherwise('/');
    }
    
})();