(function() {
    'use strict';

    angular
        .module('pgsapp.components')
        .component('footerComponent', {
                controller: function() {},
                templateUrl: 'app/components/footer/footer.directive.html'
            });

})();