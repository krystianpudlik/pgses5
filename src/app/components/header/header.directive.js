(function() {
    'use strict';

    angular
        .module('pgsapp.components')
        .component('headerComponent', {
                bindings: {
                    name: '@',
                },
                controller: function() {},
                templateUrl: 'app/components/header/header.directive.html'
        })

})();