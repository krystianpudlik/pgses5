(function () {
    'use strict';

    angular
        .module('pgsapp')
        .component('app', {
            controller: AppController,
            templateUrl: 'app/app.html'
        });


    function AppController() {
        this.text = 'I come from AppController';
    }

})();